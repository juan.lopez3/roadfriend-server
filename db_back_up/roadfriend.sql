-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-02-2015 a las 00:22:53
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `roadfriend`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `login` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin_user`
--

INSERT INTO `admin_user` (`login`, `password`, `company_id`, `type`, `active`) VALUES
('elkin lesmes', 'fcd45abceff7a654775fa1e179ab3da1046a81a77cc878248a9e64a9e1676457', 1, 2, 1),
('mocionsoft', 'fcd45abceff7a654775fa1e179ab3da1046a81a77cc878248a9e64a9e1676457', 1, 1, 1),
('Pedro', 'faeb591174704ab4ef017a9f227eab82b57620c477d1dee68b6c228aa2a78b63', 3, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_user_type`
--

CREATE TABLE IF NOT EXISTS `admin_user_type` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `admin_user_type`
--

INSERT INTO `admin_user_type` (`id`, `name`, `description`) VALUES
(1, 'ADMINISTRATOR', 'All permissions'),
(2, 'CLIENT', 'I only can see my company');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `company`
--

CREATE TABLE IF NOT EXISTS `company` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `phone` varchar(10) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `company`
--

INSERT INTO `company` (`id`, `name`, `phone`) VALUES
(1, 'Mocion S.A.', '5451988'),
(2, 'admin', 'admin'),
(3, '123 towing', '3052221111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `device`
--

CREATE TABLE IF NOT EXISTS `device` (
  `user_id` varchar(10) NOT NULL,
  `registration_id` varchar(300) NOT NULL,
  `application` varchar(7) NOT NULL,
  `platform` varchar(7) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `driver`
--

CREATE TABLE IF NOT EXISTS `driver` (
`id` bigint(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `plates` varchar(10) NOT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `driver`
--

INSERT INTO `driver` (`id`, `name`, `email`, `password`, `phone`, `plates`, `available`, `active`) VALUES
(1, 'Elkin Lesmes', 'elkin.lesmes@mocionsoft.com', '0981d7992e7f6b7f365d', '3188146428', 'RAV-666', 1, 1),
(3, 'Edward Klauss', 'edward@klauss.com', '0981d7992e7f6b7f365d', '3003203131', 'TRX 555', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `general`
--

CREATE TABLE IF NOT EXISTS `general` (
  `name` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `general`
--

INSERT INTO `general` (`name`, `value`) VALUES
('BANNER_URL', '#');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` varchar(25) NOT NULL,
  `vehicle_plates` char(7) DEFAULT NULL,
  `vehicle_position` varchar(100) DEFAULT NULL,
  `client_phone` varchar(10) NOT NULL,
  `client_address` varchar(100) NOT NULL,
  `client_position` varchar(100) DEFAULT NULL,
  `request_hour` datetime DEFAULT NULL,
  `response_hour` datetime DEFAULT NULL,
  `serve_hour` datetime DEFAULT NULL,
  `cancel_hour` datetime DEFAULT NULL,
  `service_interval` varchar(20) DEFAULT NULL,
  `service_type_id` tinyint(4) NOT NULL,
  `company_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `driver_comment` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_type`
--

CREATE TABLE IF NOT EXISTS `service_type` (
  `id` tinyint(4) NOT NULL,
  `category` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `service_type`
--

INSERT INTO `service_type` (`id`, `category`, `type`) VALUES
(1, 'towing', 'accident'),
(2, 'towing', 'brake down'),
(3, 'towing', 'car for junk'),
(4, 'locksmith', 'locksmith'),
(5, 'road assistence', 'flat tire'),
(6, 'road assistence', 'fuel delivery'),
(7, 'road assistence', 'other'),
(8, 'designated driver', 'driver'),
(9, 'designated driver', 'towing');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service_type_inscription`
--

CREATE TABLE IF NOT EXISTS `service_type_inscription` (
`id` bigint(20) NOT NULL,
  `vehicle_plates` varchar(7) NOT NULL,
  `service_type_id` tinyint(4) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `service_type_inscription`
--

INSERT INTO `service_type_inscription` (`id`, `vehicle_plates`, `service_type_id`) VALUES
(1, 'NES-256', 1),
(2, 'NES-256', 2),
(3, 'NES-256', 3),
(4, 'AES-216', 5),
(5, 'AES-216', 6),
(6, 'AES-216', 7),
(7, 'AES-216', 8),
(8, 'AES-216', 9),
(9, 'NES-256', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` tinyint(4) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `status`
--

INSERT INTO `status` (`id`, `status`) VALUES
(1, 'requested'),
(2, 'rejected'),
(3, 'canceled'),
(4, 'attended'),
(5, 'closed');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicle`
--

CREATE TABLE IF NOT EXISTS `vehicle` (
  `plates` varchar(10) NOT NULL,
  `keylock` varchar(100) NOT NULL,
  `model` varchar(50) NOT NULL,
  `company_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vehicle`
--

INSERT INTO `vehicle` (`plates`, `keylock`, `model`, `company_id`, `active`) VALUES
('121212', 'ee5cd7d5d96c8874117891b2c92a036f96918e66c102bc698ae77542c186f981', 'Pedro', 3, 0),
('123456', 'fcd45abceff7a654775fa1e179ab3da1046a81a77cc878248a9e64a9e1676457', 'ajaja', 1, 1),
('1234567', 'e7bf93b428bead01dad66ac3e48f9b652f8fa9e6a741946ae58b5cacf0e7c31f', 'Ford', 1, 0),
('141414', 'ee5cd7d5d96c8874117891b2c92a036f96918e66c102bc698ae77542c186f981', 'Pedro', 2, 0),
('AES-216', '0981d7992e7f6b7f365d', 'Toyota H-400', 1, 1),
('NES-256', 'fcd45abceff7a654775fa1e179ab3da1046a81a77cc878248a9e64a9e1676457', 'Toshiba Road 504', 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_user`
--
ALTER TABLE `admin_user`
 ADD PRIMARY KEY (`login`), ADD KEY `company_id` (`company_id`), ADD KEY `type` (`type`);

--
-- Indices de la tabla `admin_user_type`
--
ALTER TABLE `admin_user_type`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `company`
--
ALTER TABLE `company`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `driver`
--
ALTER TABLE `driver`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service`
--
ALTER TABLE `service`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_service_type_id2` (`service_type_id`), ADD KEY `fk_status_id` (`status`);

--
-- Indices de la tabla `service_type`
--
ALTER TABLE `service_type`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `service_type_inscription`
--
ALTER TABLE `service_type_inscription`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_vehicle_plates` (`vehicle_plates`), ADD KEY `fk_service_type_id` (`service_type_id`);

--
-- Indices de la tabla `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehicle`
--
ALTER TABLE `vehicle`
 ADD PRIMARY KEY (`plates`), ADD KEY `fk_company_id` (`company_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `company`
--
ALTER TABLE `company`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `driver`
--
ALTER TABLE `driver`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `service_type_inscription`
--
ALTER TABLE `service_type_inscription`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `admin_user`
--
ALTER TABLE `admin_user`
ADD CONSTRAINT `fk:admin_user:admin_user_type` FOREIGN KEY (`type`) REFERENCES `admin_user_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk:admin_user:company` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `service`
--
ALTER TABLE `service`
ADD CONSTRAINT `fk_service_type_id2` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_status_id` FOREIGN KEY (`status`) REFERENCES `status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `service_type_inscription`
--
ALTER TABLE `service_type_inscription`
ADD CONSTRAINT `fk_service_type_id` FOREIGN KEY (`service_type_id`) REFERENCES `service_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_vehicle_plates` FOREIGN KEY (`vehicle_plates`) REFERENCES `vehicle` (`plates`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `vehicle`
--
ALTER TABLE `vehicle`
ADD CONSTRAINT `fk_company_id` FOREIGN KEY (`company_id`) REFERENCES `company` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
