// Put this file on same level than "node_modules" folder

/*****************************/
/********** GLOBALS **********/
/*****************************/

var DEVICES = {};
var SOCKETS = {};

var VEHICLES = {};
var SERVICES = {};
var SERVICES_REQUESTED = {};

//custom modules
var config = require('./modules/config');
var model = require('./modules/model');
var pushnotifications = require('./modules/pushnotifications');

var db = require('./modules/db');
var VehiclesMng = require('./modules/VehiclesMng');

console.log(config.mysql.user, config.mysql.password, config.mysql.host, config.mysql.port, config.mysql.database)


var apnOpts = config['apn'][config.environment];

pushnotifications.addServer("client", apnOpts.client);
pushnotifications.addServer("driver", apnOpts.driver);



var STATUS = config.STATUS;

var app = require('express')();
//var compress = require('compression')();
var cors = require('cors');
var server = require('http').createServer(app);
var opciones = {'pingInterval': 5000,'pingTimeout':8000}
var io = require('socket.io')(server, opciones);

//app.use(compress);
app.use(cors());
io.origins("*:*");
app.disable('x-powered-by');

//servidor web para verificar el estado del servicio de sockets.
app.get('/', function (req, res) {
    res.sendfile('index.html');
});


server.listen(config.server.port, config.server.ip, function () {
    console.log("The server has started on port " + config.server.port + ", ip " + config.server.ip);
    //Este ping se puede ver en la pagina de prueba index.html 
    //iniciarEmisionPeriodicaDePrueba(io);    
});



/****** ON START UP *******/

//restore the server status, loads all active services
db.getActiveServices(function (rows) {
    for (var i = 0; i < rows.length; i++) {
            SERVICES[rows[i].id] = rows[i];
    }
    checkExpiredServices();
    if (config.DEBUG) {
        //console.log(SERVICES);
    }
});



// VERIFY THE REQUEST ITEMS VALIDITY PERIODICALLY
//setInterval(checkExpiredServices, 1000 * 60 * 60 * 1);

/****************************************/
/********** EVENTS AND ACTIONS **********/
/****************************************/

io.sockets.on("connection", function (socket) {

    //VEHICLES[vehicle_plates].company
    // When a client re-open the application

    socket.on("refreshDriverStatus",function(vehicle_plates){

        if (!VEHICLES[vehicle_plates]) {
            console.log("---Emitiendo ERROR LOST.");
            socket.emit("_error", "ER_SESSION_LOST");
            return;
        } 
            
    	socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);

    	
    });
    
   socket.on('DriverMarkasBussy', function(vehicle_plates){
        VEHICLES[vehicle_plates].status     = config.VEHICLE_STATUS.BUSSY;
        socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);
    });    

   socket.on('DriverMarkasAvailable', function(vehicle_plates){
        VEHICLES[vehicle_plates].status     = config.VEHICLE_STATUS.AVAILABLE;
        socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);
    });    
    
    socket.on("refreshService",function(service){

    	var serverService = service;
    	
    	if (service && typeof service.id !== 'undefined' && SERVICES[service.id]){
    		serverService = SERVICES[service.id];
    	}else{
    		serverService = service;
    		serverService.id = null;
    		serverService.status = null;
    	}

    	socket.emit('serviceUpdate', serverService);
    	

    });

    socket.on("reconnectClient", function (service_id) {

        console.log("un cliente se reconecto: ");
        console.log(SERVICES[service_id]);

        if (!SERVICES[service_id]) {
            socket.emit("_error", "ER_SERVICE_EXPIRED");
            return;
        }
        socket.room = 'CLIENTS';
        socket.join('CLIENTS');

        SOCKETS[SERVICES[service_id].client_phone] = socket;
        // echo to client they've made a service request
        socket.emit('reconnected');

    });

    // When a client request a service
    socket.on("requestService", function (request) {

        // Compatibilidad para clientes viejos
        request.client_phone = request.client_phone || request.phone;
        request.client_address = request.client_address || request.address;
        request.client_position = request.client_position || request.position;

        var service = new model.SERVICE(request.client_phone);
        service.client_phone    = request.client_phone;
        service.client_address  = request.client_address;
        service.client_position = request.client_position;
        service.service_type_id = request.service_type_id;
        service.service_type = config.SERVICE_TYPES[request.service_type_id];
        
        
        service.company_id = request.company_id || null; // company id if request comes from company

        //si no existen vehiculos prestando serivicios rechazamos el servicio
        console.log("cantidad vehiculos" + Object.keys(VEHICLES).length);

        if (Object.keys(VEHICLES).length > 0){
        	service.status = STATUS.REQUESTED;	
        	service.message = "Searching driver";
        }else{
        	service.status = STATUS.CANCELED;
        	service.message = "No vehicles available";
        }

        db.serviceRegister(service);

        //si no hay carros, no se puede prestar el servicio
        if (service.status == STATUS.CANCELED) {
            socket.emit("_error", "ER_NO_VEHICLES_AVAILABLE");
            service.id = null;
            socket.emit("serviceUpdate",service);
            return;
        }

        socket.room = 'CLIENTS';
        SOCKETS[service.client_phone] = socket;

        SERVICES[service.id] = service;
        SERVICES_REQUESTED[service.id] = service;

        // echo to client they've made a service request
        socket.emit('requestMadeSuccessfully', service.id);
        socket.emit('serviceUpdate', service);
        
        // echo to VEHICLES that a person has connected to clients room
        //socket.broadcast.to('VEHICLES').emit('requestMade', SERVICES_REQUESTED)
        console.log("Revisando si enviamos push notification");
        Object.keys(VEHICLES).forEach(function (key) {
            
            var vehicle = VEHICLES[key];
            console.log("estado", vehicle.plates, vehicle.status,vehicle.position);
            
            //Revisamos si el vehiculo presta el servicio
            if (!(VehiclesMng.vehicleHasServiceType(vehicle, service.service_type_id))){
                return;
            } 
            //Revisamos si el vehiculo esta disponible a prestar servicio
            if (vehicle.status !== config.VEHICLE_STATUS.AVAILABLE ){
                return;
            }

            //Revisamos si el vehiculo esta cerca en el rango de distancia de prestación del servicio
            if (vehicle.position) {
                var pos = service.client_position.split(",");
                var distance = (pos[1]) ? getDistanceFromLatLon(vehicle.position.latitude, vehicle.position.longitude, pos[0], pos[1]) : null;
                console.log("distancia a servicio: " + distance);

                if (distance !== null && DEVICES[vehicle.plates] && distance && distance < config.DISTANCE_LIMIT_REQUEST) {
                    pushnotifications.send("New service requested", "New service requested ", DEVICES[vehicle.plates]);
                }
            }

        });


        socket.broadcast.emit('requestMade', SERVICES_REQUESTED);

        console.log("Broadcasting:  Service request made: " + request.phone + ". Socket =", socket.id);
    });


 
    // When a driver select a request from the list to attend it
    socket.on("attendRequest", function (service_id, vehicle_plates, interval, vehicle_position) {

        var service = SERVICES[service_id];

        //Verificamos que todo este dispuesto para prestar el servicio
        if (!(service.status == STATUS.REQUESTED && vehicle_plates)) {
            socket.emit("_error", "ER_ATTEND_FAILED");
            return;
        }

        service.vehicle_plates = vehicle_plates;
        service.vehicle_position = vehicle_position;
        service.service_interval = interval;
        service.status = STATUS.ATTENDED;
        service.response_hour = new Date().toString();
        service.update_hour = "response_hour=NOW()";
        db.serviceUpdate(service);
        service.company = VEHICLES[vehicle_plates].company;


        SERVICES[service_id] = service;
        delete SERVICES_REQUESTED[service_id];

        console.log(service.client_phone);
        console.log(vehicle_plates);

        VEHICLES[vehicle_plates].status     = config.VEHICLE_STATUS.ATTENDING;
        VEHICLES[vehicle_plates].service = SERVICES[service_id];

        //nuevo VEHICLES[vehicle_plates].company, 
        socket.to(SOCKETS[service.client_phone].id).emit("serviceUpdate", service);
        //viejo
        socket.to(SOCKETS[service.client_phone].id).emit("requestAttended", VEHICLES[vehicle_plates].company, service);
        
        pushnotifications.send(service.vehicle_plates + " is going to assist you.", service.vehicle_plates + "is going to assist you.", DEVICES[service.client_phone]);

        // emit notification to the driver
        //viejo
        socket.emit("requestAttended", service);
        
        //nuevo
        socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);
        
        //socket.broadcast.to('VEHICLES').emit('refreshRequestList', SERVICES_REQUESTED);
        
        socket.broadcast.emit('refreshRequestList', SERVICES_REQUESTED);


    });

    // When a driver cancel the request previously taken
    socket.on("cancelRequestByDriver", function (service_id, comment) {
        //update service
        if (!SERVICES[service_id]) {
            socket.emit("requestCancelledByDriver");
            socket.broadcast.emit('refreshRequestList', SERVICES_REQUESTED);
            return;
        }

        var service = SERVICES[service_id];
        var vehicle_plates = service.vehicle_plates;
        
        VEHICLES[service.vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
        VEHICLES[service.vehicle_plates].service = null;

        service.vehicle_plates = null;
        console.log(service.vehicle_plates);
        service.vehicle_position = '';
        service.status = STATUS.REQUESTED;
        service.driver_comment = comment;
        service.response_hour = '';
        service.update_hour = "cancel_hour=NOW()";
        db.serviceUpdate(service);

        SERVICES[service_id] = service;
        //re-insert the service in service requested list
        SERVICES_REQUESTED[service_id] = service;

        //notify to client
        if (SOCKETS[service.client_phone]){
            socket.to(SOCKETS[service.client_phone].id).emit("requestCancelledByDriver");
                    //nuevo VEHICLES[vehicle_plates].company, 
            socket.to(SOCKETS[service.client_phone].id).emit("serviceUpdate", service);
            
         }
        
    	if (DEVICES[service.client_phone])
        pushnotifications.send("Request cancelled", "Request cancelled by '" + service.vehicle_plates + "' vehicle.", DEVICES[service.client_phone]);
        //socket.broadcast.to('VEHICLES').emit('refreshRequestList', SERVICES_REQUESTED);
        
        //notificar a otros vehiculos
        socket.broadcast.emit('refreshRequestList', SERVICES_REQUESTED);
        // notify to the driver
        //socket.emit("requestCancelledByDriver");
        socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);

    });

    // When a driver finalize the service
    socket.on("closeServiceByDriver", function (service_id, vehicle_plates) {
        //update service
        
        if (SERVICES[service_id]) {
            var service = SERVICES[service_id];
           
            
            vehicle_plates = service.vehicle_plates;
            
            service.status = STATUS.CLOSED;
            service.serve_hour = new Date().toString();
            service.update_hour = "serve_hour=NOW()";
            
            db.serviceUpdate(service);

            //notify to client
            pushnotifications.send("Service finished", "Service complete. Thanks for using Road Friend", DEVICES[service.client_phone]);
            
            socket.to(SOCKETS[service.client_phone].id).emit("serviceClosedByDriver"); //Viejo
            socket.to(SOCKETS[service.client_phone].id).emit("serviceUpdate", service);//Nuevo
            
            //Esperamos a borrarlo para que el usuario pueda ver el servicio cerrado en la aplicación
            setTimeout(function(){ 
                delete SERVICES[service_id];
                
            }, 30000);
            //

        } 
        VEHICLES[vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
        //socket.emit("serviceClosedByDriver");
        socket.emit('driverUpdate', VEHICLES[vehicle_plates], SERVICES_REQUESTED);
    });


    socket.on("clientCancelService",function(serviceClient){
        console.log("el cliente cancelo el servicio: " + serviceClient.id);
        var service_id = serviceClient.id;

        if (!SERVICES[service_id]){
        	socket.emit("serviceUpdate",{});
        }

            var service = SERVICES[service_id];
            service.status = STATUS.CANCELED;
            service.update_hour = "cancel_hour=NOW()";
            db.serviceUpdate(service);

            //Si un conductor estaba prestando el servicio
            if (service.vehicle_plates) {
                socket.to(SOCKETS[service.vehicle_plates].id).emit("requestCancelledByClient");
                pushnotifications.send("Service cancelled", "Service cancelled by " + service.client_phone + " client.", DEVICES[service.vehicle_plates]);
            
            	if (VEHICLES[service.vehicle_plates])
                VEHICLES[service.vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
            }

            service.message = "Service Canceled";
            // notify to the client
            socket.emit("serviceUpdate",service);

            delete SERVICES_REQUESTED[service_id];
            delete SERVICES[service_id];

            //refresh request list on VEHICLES
            //socket.broadcast.to('VEHICLES').emit('refreshRequestList', SERVICES_REQUESTED);
            socket.broadcast.emit('refreshRequestList', SERVICES_REQUESTED);
        

    });

    // When a client cancel the request //
    socket.on("cancelRequestByClient", function (service_id) {


        console.log("el cliente cancelo el servicio: " + service_id);
        //socket.broadcast.emit('test', 'se cancelo el servicio por el cliente');

        if (SERVICES[service_id]) {
            var service = SERVICES[service_id];
            if (service.vehicle_plates && VEHICLES[service.vehicle_plates]) {
                VEHICLES[service.vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
            }
            service.status = STATUS.CANCELED;
            service.update_hour = "cancel_hour=NOW()";
            db.serviceUpdate(service);

            //Si un conductor estaba prestando el servicio
            if (service.vehicle_plates) {
                socket.to(SOCKETS[service.vehicle_plates].id).emit("requestCancelledByClient");
                pushnotifications.send("Service cancelled", "Service cancelled by " + service.client_phone + " client.", DEVICES[service.vehicle_plates]);
            }

            delete SERVICES_REQUESTED[service_id];
            delete SERVICES[service_id];
            // notify to the client
            socket.emit("requestCancelledByClient");
            //refresh request list on VEHICLES
            //socket.broadcast.to('VEHICLES').emit('refreshRequestList', SERVICES_REQUESTED);
            socket.broadcast.emit('refreshRequestList', SERVICES_REQUESTED);
        } else {
            socket.emit("_error", "ER_ALREADY_CANCELLED");
        }
    });

    // When a client finalize the service
    socket.on("clientServiceCompleted", function (service) {
        //update service
        var service_id = service.id;

        //El servicio no existe
        if (!SERVICES[service_id]) {
        	service.status  = service.TYPE_SELECTION;
        	service.message = "Service doesn't exist"; 
        	socket.emit("serviceUpdate",service);
        }

        service = SERVICES[service_id];

        if (service.status == STATUS.ATTENDED) {
            VEHICLES[service.vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
            service.status = STATUS.CLOSED;
            service.serve_hour = new Date().toString();
            service.update_hour = "serve_hour=NOW()";
            service.message = "Service successfully closed, Thanks for using roadfriend";
            db.serviceUpdate(service);
            //notify to driver
            socket.to(SOCKETS[service.vehicle_plates].id).emit("serviceClosedByClient");
            pushnotifications.send("Service successfully closed", " Service successfully closed by " + service.client_phone + " client.", DEVICES[service.vehicle_plates]);
            
            socket.emit("serviceUpdate",service);
            //remove the service from the requested list
            delete SERVICES_REQUESTED[service_id];
            delete SERVICES[service_id];
        } else {
        	//no esta en un estado que podamos cerrar
        	service.message = "Service could not be closed";
        	socket.emit("serviceUpdate",service); 

        }

    });
    socket.on("closeServiceByClient", function (service_id) {
        //update service
        var service = SERVICES[service_id];
        if (SERVICES[service_id]) {
            var service = SERVICES[service_id];

            if (service.status == STATUS.ATTENDED) {
                VEHICLES[service.vehicle_plates].status = config.VEHICLE_STATUS.AVAILABLE;
                service.status = STATUS.CLOSED;
                service.serve_hour = new Date().toString();
                service.update_hour = "serve_hour=NOW()";
                db.serviceUpdate(service);

                //remove the service from the requested list
                delete SERVICES_REQUESTED[service_id];
                delete SERVICES[service_id];as 
                //notify to driver
                socket.to(SOCKETS[service.vehicle_plates].id).emit("serviceClosedByClient");
                pushnotifications.send("Service successfully closed", " Service successfully closed by " + service.client_phone + " client.", DEVICES[service.vehicle_plates]);
                socket.emit("serviceClosedByClient");
            } else {
                socket.emit("_error", "ER_CANT_CLOSE");
            }
        }
    });

    // When a driver gets back from the busy status and wants to refresh his request list
    socket.on("getRequestList", function (plates, ubicacion, fn) {

        console.log("solicitaron la lista de servicios", plates, ubicacion);
        if (plates && ubicacion && typeof plates !== "funcion") {
            VEHICLES[plates]['position'] = ubicacion;
            //Si solicitan la lista de servicios es porque esta disponible
            VEHICLES[plates].STATUS = config.VEHICLE_STATUS.AVAILABLE;
        }

        socket.emit("refreshRequestList", SERVICES_REQUESTED);
        if (typeof fn == "function")
            fn(SERVICES_REQUESTED);
    });


    // When a driver starts session
    socket.on("driverLogin", function (credentials) {
        
        console.log("Driver intentando loguearse");
        var contectado = (typeof SOCKETS[credentials.plates] !== "undefined" && SOCKETS[credentials.plates].connected);
        console.log(socket.id);
        if ( typeof SOCKETS[credentials.plates] !== "undefined")
            console.log(SOCKETS[credentials.plates].id);

        
        
        if (VEHICLES[credentials.plates]) {
            logoutDriver(credentials.plates);
            /*
            if (contectado){
                console.log("ya existe un driver activo");
                socket.emit("_error", "ER_ALREADY_LOGGED");
                return;
            }else{
                console.log("el driver esta inactivo");
                logoutDriver(credentials.plates);
            }*/
            //io.sockets.sockets[a_socket_id]!=undefined
            //SOCKETS[credentials.plates].connected
        }
        
        VehiclesMng.getVehicle(credentials, function (vehicle) {         

            //onerror socket.emit("_error", response.error.code);
            if (!vehicle) {
                socket.emit("_error", "ER_BAD_LOGIN");
                return;
            }

        //Nos logueamos satisfactoriamente  u
        //Miramos si el vehiculo logueado tiene un servicio activo y lo devolvemos junto con este

            addVehicleToLoggued(vehicle, socket, function (currentService) {
                console.log("logueado vehicle");
                console.log(vehicle);
                socket.emit('driverLoggedIn', vehicle);

            });
            console.log("esto lo terminamos bien");
        });

    });


    // When a driver with open session restarts the application
    socket.on("driverLoginWithOpenSession", function (vehicle_plates) {
        
        
        console.log("---PASADA POR autodriverLogin.");
        //Verificar si otro usuario no esta logueado con la misma cuenta
        //Verificar token
        //Autorizar el usuario
        
        
        if (VEHICLES[vehicle_plates]) {
            var vehicle = VEHICLES[vehicle_plates];
            addVehicleToLoggued(vehicle, socket, function (currentService) {
                socket.emit('driverLoggedInAuto', vehicle);
            });
        } else {
            console.log("---Emitiendo ERROR LOST.");
            socket.emit("_error", "ER_SESSION_LOST");
        }

    });

    function logoutDriver(vehicle_plates){
        try {
            

            if ((VEHICLES[vehicle_plates]))
                delete VEHICLES[vehicle_plates];

            if ((DEVICES[vehicle_plates]))
                delete DEVICES[vehicle_plates];

            if ((DEVICES['driver'][vehicle_plates]))
                delete DEVICES['driver'][vehicle_plates];

            if ((SOCKETS[vehicle_plates]))
                delete SOCKETS[vehicle_plates];


            socket.leave(socket.room);
        } catch (e) {
            console.log(e.message);
        }        
    }
    // When a driver close session
    socket.on('driverLogout', function (vehicle_plates) {
        console.log("un driver se salio: " + vehicle_plates);
        logoutDriver(vehicle_plates);
        socket.emit("driverLoggedOut");

    });

    // When a node loses the connection
    socket.on("disconnect", function () {
        console.log("Connection finished: ", socket.id);
    });

    // On new registration Id
    socket.on("saveRegistriationId", function (ob) {

        console.log("un dispositivo se conecto al servicio de APN");
        var device = new model.DEVICE();
        device.registration_id = ob.registration_id;
        device.application = ob.application;
        device.platform = ob.platform;
        device.user_id = ob.user_id;

        if (!DEVICES[device.application])
            DEVICES[device.application] = {};

        switch (device.application) {
            case "client":
                DEVICES[device.user_id] = device;

                DEVICES[device.application][device.user_id] = device;
                break;
            case "driver":
                DEVICES[device.user_id] = device;
                DEVICES[device.application][device.user_id] = device;
                break;

        }

        db.setRegistrationId(ob);

        console.log(device);
        console.log("registrando un movil");

        /*setTimeout(function(){
         gcm.send("Mensaje de prueba", "mensaje de prueba", [ob.registration_id]); 
         }, 7000);*/
    });

    socket.on("removeRegistrationId", function (ob) {
        switch (ob.application) {
            case "client":
                delete DEVICES[ob.user_id];
                delete DEVICES[ob.application][ob.user_id];
                break;
            case "driver":
                delete DEVICES[ob.user_id];
                delete DEVICES[ob.application][ob.user_id];
                break;

        }

        //db.removeRegistrationId(ob);
    });



});

function addVehicleToLoggued(vehicle, socket, callback) {
    
    socket.room = 'VEHICLES';
    socket.join('VEHICLES');

    VEHICLES[vehicle.plates] = vehicle;
    SOCKETS[vehicle.plates] = socket;

    db.getServiceByPlates(vehicle.plates, STATUS.ATTENDED, function (currentService) {
        callback(currentService);
        VEHICLES[vehicle.plates].STATUS = (currentService) ? config.VEHICLE_STATUS.ATTENDING : config.VEHICLE_STATUS.AVAILABLE;
        console.log("Vehicle " + vehicle.plates + " start session. Socket =", socket.id);
    });
}

function checkExpiredServices() {
    var changes = false;
    for (i in SERVICES_REQUESTED) {
        // Elapsed time
        var time = new Date() - SERVICES_REQUESTED[i].request_hour;
        time = (((time / 1000) / 60)); //milliseconds to minutes
        time = time / 60; //minutes to hours
        // If the request is 1 hour old then cancel it.
        if (time < 5)
            continue;
        //no se pueden cancelar servicios que esten siendo atendidos
        if (SERVICES_REQUESTED[i].status == STATUS.ATTENDED)
            continue;

        cancel_service(SERVICES_REQUESTED[i]);
        changes = true;
    }
    // if changes refresh the request list for drivers
    if (changes) {
        io.sockets.emit("refreshRequestList", SERVICES_REQUESTED);
    }
}

function cancel_service(service) {

    service.status = STATUS.CANCELED;
    service.cancel_hour = new Date().toString();
    service.update_hour = "cancel_hour=NOW()";
    db.serviceUpdate(service);

    io.sockets.to(service.client.socket_id).emit("_error", "ER_SERVICE_EXPIRED");
    pushnotifications.send("Expired request", "Expired request. Your request has expired. Try again.", service.client.device);

    // removes the actual service from REQUESTED list
    delete SERVICES_REQUESTED[service.id];
    delete SERVICES[service.id];
}


/* 
 * **********************************************************************
 * Ping de prueba para saber que esta funcionando la conexión de socket.io 
 * Este ping se puede ver en la pagina de prueba index.html 
 * */
function iniciarEmisionPeriodicaDePrueba(io) {

    // Send current time to all connected clients
    function sendTime() {
        io.sockets.emit('time', {time: new Date().toJSON()});
    }
    // Send current time every 10 secs 
    setInterval(sendTime, 10000);
}


getDistanceFromLatLon = function (lat1, lon1, lat2, lon2) {
    var R = 6371000; // Radius of the earth in meters
    var dLat = deg2rad(lat2 - lat1);  // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d;
};

deg2rad = function (deg) {
    return deg * (Math.PI / 180);
};