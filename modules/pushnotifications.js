var pushnotifications = (function(){

	var my = {};
	
	var gcm = require('./gcm');
        var apn = require('./apn');
        
	var apps = {};
        
        my.addServer = function(app_name, options){
            
            options = options || {};
                apps[app_name] = {};
                apps[app_name]['ios']     = apn(options);
                apps[app_name]['android'] = gcm;
        };
        
	
	my.send = function(title, text, device) {  
            console.log("pushnotification. enviando mensaje:"+text);
            console.log(device);
            if (!(device && device.platform && device.application)){
                return;
            }
            
            apps[device.application][device.platform].send(title,text,device.registration_id);
	};
	
	return my;
}());

module.exports = pushnotifications;