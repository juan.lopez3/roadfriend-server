module.exports = {
    DEBUG: true,
    DISTANCE_LIMIT_REQUEST : 25000,
    environment: "prod",
    STATUS: {
        TYPE_SELECTION: -3,
        PHONE_SELECTION: -2,
        CREATING:0,
        REQUESTED: 1,
        REJECTED: 2,
        CANCELED: 3,
        ATTENDED: 4,
        CLOSED: 5
    },
    
    TYPES_CODES : {
    TOWING_ACCIDENT:   1,
    TOWING_BRAKEDOWN:  2,
    TOWING_CARFORJUNK: 3,
    LOCKSMITH_LOCKSMITH: 4,
    ASSISTANCE_FLAT:   5,
    ASSISTANCE_FUEL:   6,
    ASSISTANCE_OTHER:  7,
    DRIVER_DRIVER:     8,
    DRIVER_TOWING:     9

},

   SERVICE_TYPES : {
    /* { id: 0,category: '---',type: '---' },*/
      "1": {id: 1, category: 'towing', type: 'accident'}
    , "2": {id: 2, category: 'towing', type: 'brake down'}
    , "3": {id: 3, category: 'towing', type: 'car for junk'}
    , "4": {id: 4, category: 'locksmith', type: 'locksmith'}
    , "5": {id: 5, category: 'road assistence', type: 'flat tire'}
    , "6": {id: 6, category: 'road assistence', type: 'fuel delivery'}
    , "7": {id: 7, category: 'road assistence', type: 'other'}
    , "8": {id: 8, category: 'designated driver', type: 'driver'}
    , "9": {id: 9, category: 'designated driver', type: 'towing'}
},    


    VEHICLE_STATUS: {
            AVAILABLE: 1,
            ATTENDING: 2,
            BUSSY:3,
            SERVICEFINISHED:4,
    },
    server: {
        ip:   process.env.OPENSHIFT_NODEJS_IP  || "127.0.0.1",
        port: process.env.OPENSHIFT_NODEJS_PORT || 9000
    },
    mysql: {
        
        user: process.env.OPENSHIFT_MYSQL_DB_USERNAME     || process.env.OPENSHIFT_EXTMYSQL_DB_USERNAME || 'admin5vUDr3S',
        password: process.env.OPENSHIFT_MYSQL_DB_PASSWORD || process.env.OPENSHIFT_EXTMYSQL_DB_PASSWORD ||  'b45h6DK6zc17',
        host: process.env.OPENSHIFT_MYSQL_DB_HOST         || process.env.OPENSHIFT_EXTMYSQL_DB_HOST || 'localhost',
        port: process.env.OPENSHIFT_MYSQL_DB_PORT         || process.env.OPENSHIFT_EXTMYSQL_DB_PORT || 51876,
        database: process.env.OPENSHIFT_APP_NAME          || process.env.OPENSHIFT_EXTMYSQL_DB_NAME || 'serverroadfriend'
        
    },
    gcm: {
        apiKey: 'AIzaSyCEzQvz0ztTeKJ0ID3tmnoodn8LJDiFpBQ'
    },
    //see this tutorial - http://www.raywenderlich.com/32960/apple-push-notification-services-in-ios-6-tutorial-part-1
    apn: {
        //gateway: 'gateway.sandbox.push.apple.com', // this URL is different for Apple's Production Servers and changes when you go to production
        prod: {
            client: {
                production: true,
                cert: __dirname + '/ioscerts/client/prod/roadfriendProdClientCert.pem', // Expires: 30-ene-2016
                key:  __dirname + '/ioscerts/client/prod/roadfriendProdClientKey.pem', // Expires: 30-ene-2016
                ca:  __dirname + '/ioscerts/entrust_2048_ca.cer'
            }
            ,driver: {
                production: true,
                cert: __dirname + '/ioscerts/parner/prod/roadfriendProdParnerCert.pem', // Expires: 30-ene-2016
                key: __dirname + '/ioscerts/parner/prod/roadfriendProdParnerKey.pem',
                ca:  __dirname + '/ioscerts/entrust_2048_ca.cer'// Expires: 30-ene-2016
            }
        },
        
        dev: {
            client: {
                production: false,
                cert: __dirname + '/ioscerts/client/dev/roadfriendDevClientCert.pem', // Expires: 30-ene-2016
                key: __dirname + '/ioscerts/client/dev/roadfriendDevClientKey.pem',
                ca:  __dirname + '/ioscerts/entrust_2048_ca.cer'// Expires: 30-ene-2016
            }
            ,driver: {
                production: false,
                cert: __dirname + '/ioscerts/parner/dev/roadfriendDevParnerCert.pem', // Expires: 30-ene-2016
                key: __dirname + '/ioscerts/parner/dev/roadfriendDevParnerKey.pem',
                ca:  __dirname + '/ioscerts/entrust_2048_ca.cer'// Expires: 30-ene-2016
            }
        }


    }

};


