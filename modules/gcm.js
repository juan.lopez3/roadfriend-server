  /***************************************/
 /********** GCM SERVER MODULE **********/
/***************************************/

var gcm = (function(){

	var my = {};
	var config = require('./config');
	var gcm = require('node-gcm');
	var sender = new gcm.Sender(config.gcm.apiKey);
	
	my.registrationIds = [];
	
	// REG_ID_IS_THE_REGISTRATION_ID_THAT_WAS_GENERATED_BY_GCM
	my.register = function(registration_id) {
		my.registrationIds.push(registration_id);
	}
	
	my.unregister = function(registration_id) {
		delete my.registrationIds[registration_id];
	}
	
	my.send = function(title, text, devices) {
		// Value the payload data to send...
		var message = new gcm.Message();
		message.addData('message', text);
		message.addData('title', title);
		message.addData('msgcnt','1'); // Shows up in the notification in the status bar
		message.addData('soundname','sound/job-done.mp3'); //Sound to play upon notification receipt - put in the www folder in app
		//message.collapseKey = 'demo';
		message.delayWhileIdle = false; // If the device is connected but idle, the message will be delivered unless the delay_while_idle flag is set to true.
		message.timeToLive = 3600;// Duration in seconds to hold in GCM and retry before timing out. Default 4 weeks (2,419,200 seconds) if not specified.
		/** Parameters: message-literal, registrationIds-array, No. of retries, callback-function **/
		sender.send(message, devices, 4, function (err, result) {
			if(err){
				console.log(err);
			} else {
				console.log(result);
			}
		});
	};
	
	return my;
}());

module.exports = gcm;