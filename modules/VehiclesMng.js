var vh = (function () {


    var db = require('./db');
    var model = require('./model');

    var my = {};

    var vehicles = {};


    my.getVehicle = function (credentials, callback) {
        db.getVehicle(credentials, function (response) {
            //No hay vehiculos

            if (response === null || !response.plates) {
                callback(null);
                return;
            }

            // create the driver instance
            vh = new model.VEHICLE();
            vh.plates = response.plates;
            vh.model = response.model;
            vh.company_id = response.company_id;
            vh.company = response.company;
            vh.active = response.active;

            // get the service type list of this vehicle
            db.getServiceTypesByPlates(vh.plates, function (service_type_list) {
                vh.service_types = service_type_list;
                callback(vh);
            });



        });
    };

    my.vehicleHasServiceType = function (vehicle, service_id) {
        
        console.log("revisando si soporta el tipo: "+service_id);
        
        if (!(vehicle && vehicle.service_types)) {
            return false;
        }
        console.log("servicios del vehiculo");
        for (var i = 0; i < vehicle.service_types.length; i++) {
            console.log(vehicle.service_types[i].service_type_id);
            if (parseInt(vehicle.service_types[i].service_type_id) == parseInt(service_id)) {
                return true;
            }

        }
        return false;
    };

    return my;
}());

module.exports = vh;