/****************************************/
/********** PERSISTENCE MODULE **********/
/****************************************/

var db = (function () {
    var my = {};
    var crypto = require('./crypto');
    var config = require('./config');
    var mysql = require("mysql");
    //config.mysql.debug = true;
    var pool = mysql.createPool(config.mysql);

    //comprobamos que tengamos acceso a la base de datos
    var testConnection = function (callback) {
        pool.getConnection(function (err, connection) {
            if (err != null && err != undefined) {
                console.log("database_error", err);
                process.exit(1);
            }
        });
    };
    testConnection();

    my.serviceRegister = function (ob) {
        var company_id = (ob.company_id) ? ob.company_id : "NULL";
        executeQuery("INSERT INTO service (id, client_phone, client_address, client_position, request_hour, service_interval, service_type_id, company_id, status) VALUES ('" + ob.id + "'," + ob.client_phone + ",'" + ob.client_address + "','" + ob.client_position + "', NOW(),'" + ob.service_interval + "'," + ob.service_type_id + "," + company_id + "," + ob.status + ")",
                function (res) { /* Do nothing after execution */
                });
    };

    my.serviceUpdate = function (ob) {

        vehicle_plates = (!ob.vehicle_plates || ob.vehicle_plates.toUpperCase() === 'NULL' || ob.vehicle_plates === '') ? "NULL" : "'" + ob.vehicle_plates + "'";
        console.log(vehicle_plates);
        executeQuery("UPDATE service SET vehicle_plates=" + vehicle_plates + ", vehicle_position='" + ob.vehicle_position + "', client_phone='" + ob.client_phone + "', client_address='" + ob.client_address + "', client_position='" + ob.client_position + "', " + ob.update_hour + ", service_interval='" + ob.service_interval + "', service_type_id=" + ob.service_type_id + ", status=" + ob.status + ", driver_comment='" + ob.driver_comment + "' WHERE id='" + ob.id + "'",
                function (res) {
                    console.log("Servicio disponible");
                });
    }

    my.getVehicle = function (ob, callback) {
        executeQuery("SELECT plates, model, company_id, active FROM vehicle WHERE plates='" + ob.plates + "' AND keylock='" + crypto.encrypt(ob.keylock) + "' AND active=1",
                function (response) {
                    
                    if (!response || response.length <=0){
                        callback(null);return;
                    }

                    if (response.error){
                        callback(null);return;  
                    }

                     my.getCompany(response[0].company_id, function(company){
                         response[0].company = company; 
                         callback(response[0]);
                     });
                    
                });
    };

    my.getServices = function (callback) {
        executeQuery("SELECT id, vehicle_plates, vehicle_position, client_phone, client_address, client_position, request_hour, response_hour, serve_hour, cancel_hour, service_interval, service_type_id, company_id, status FROM service",
                function (response) {
                    callback(response);
                });
    };

    my.getActiveServices = function (callback) {
        var query = "SELECT service.id, vehicle_plates, vehicle_position, client_phone, client_address, client_position, request_hour, response_hour, serve_hour, cancel_hour, service_interval, service_type_id, service.company_id, status, company.name as company_name, company.phone as company_phone  FROM service ";
        query += " JOIN vehicle ON vehicle.plates = service.vehicle_plates";
        query += " JOIN company ON company.id = vehicle.company_id";

        query += " WHERE (status = " + config.STATUS.ATTENDED + " OR status = " + config.STATUS.REQUESTED+")";
        query += " AND (service.vehicle_plates IS NOT NULL)";

        executeQuery(query,function (response) {

                    for (var i = 0; i < response.length; i++) {
                        response[i].company = {name: response[i].company_name ,phone:response[i].company_phone};
                        response[i].service_type = config.SERVICE_TYPES[response[i].service_type_id];
                        //Do something
                    }                    

                    callback(response);
                });
    }

    my.getServiceByPlates = function (plates, _status, callback) {
        executeQuery("SELECT id, vehicle_plates, vehicle_position, client_phone, client_address, client_position, request_hour, response_hour, serve_hour, cancel_hour, service_interval, service_type_id, company_id, status FROM service WHERE vehicle_plates='" + plates + "' AND status=" + _status,
                function (response) {
                    callback(response[0] || undefined);
                });
    }

    my.getCompanies = function (callback) {
        executeQuery("SELECT id, name, phone FROM company",
                function (response) {
                    callback(response);
                });
    }

    my.getCompany = function (company_id, callback) {
        executeQuery("SELECT id, name, phone FROM company WHERE id="+company_id,
                function (response) {
                    callback(response[0] || null);
                });
    };

    my.getServiceTypesByPlates = function (vehicle_plates, callback) {
        executeQuery("SELECT service_type_id FROM service_type_inscription WHERE vehicle_plates = '" + vehicle_plates + "'",
                function (response) {
                    callback(response);
                });
    }

    /* entity driver is deprecated */
    my.driverLogin = function (ob, callback) {
        executeQuery("SELECT id,name,email,phone,plates,active FROM driver WHERE email='" + ob.email + "' AND password='" + crypto.encrypt(ob.password) + "' AND active=1",
                function (response) {
                    callback(response);
                });
    }

    my.driverRegister = function (ob, callback) {
        executeQuery("INSERT INTO driver (name, email, password, phone, plates, active) VALUES ('" + ob.name + "','" + ob.email + "','" + crypto.encrypt(ob.password) + "','" + ob.phone + "','" + ob.plates + "', 1, 1)",
                function (response) {
                    /* register the service types selected */
                    if (!response.error && ob.services.length > 0) {
                        //building the insert query for multiple rows
                        var query = "INSERT INTO service_type_inscription (vehicle_plates, service_type_id) VALUES ";
                        for (var i = 0; i < ob.services.length; i++) {
                            query += "(" + response.insertId + ", " + ob.services[i] + "),";
                        }
                        query = query.substring(0, query.length - 1); //removes the last comma
                        executeQuery(query, function (r) {
                        });
                    }
                    callback(response);
                });
    }

    // Push Notifications
    my.getDevices = function (callback) {
        executeQuery("SELECT user_id, registration_id, application, platform FROM device",
                function (response) {
                    callback(response);
                });
    }
    // Push Notifications
    my.setRegistrationId = function (ob) {
        executeQuery("SELECT count(*) AS count FROM device WHERE user_id='" + ob.user_id + "'",
                function (devices) {
                    if (devices[0].count > 0) {
                        executeQuery("UPDATE device SET registration_id='" + ob.registration_id + "', application='" + ob.application + "', platform='" + ob.platform + "' WHERE user_id='" + ob.user_id + "'",
                                function (res) { /* Do nothing after execution */
                                });
                    } else {
                        executeQuery("INSERT INTO device (user_id, registration_id, application, platform) VALUES ('" + ob.user_id + "','" + ob.registration_id + "','" + ob.application + "','" + ob.platform + "')",
                                function (res) { /* Do nothing after execution */
                                });
                    }
                });
    }

    function executeQuery(query, callback) {
        //use pool connection
        pool.getConnection(function (error, connection) {
            //gets error on connection
            if (error) {
                callback({'error': error});
            }
            //execute the query
            connection.query(query, function (error, response, field) {
                //It's important release the connection after every query
                connection.release();
                //gets error on query
                if (error) {
                    console.log("SQL error: ", query + "   =>  " + error);
                    callback({'error': error}); //return the error	
                }
                else
                    callback(response);	//return the result		
            });
        });
    }

    return my;
}());

module.exports = db;
