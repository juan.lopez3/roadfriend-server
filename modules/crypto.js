  /*****************************************/
 /********** CRYPTOGRAPHY MODULE **********/
/*****************************************/

var crypto = (function(){
	var my = {};
	var cryptography = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'mocionsoft';
	
	my.encrypt = function(text){
	  return cryptography.createHash('sha256').update(text).digest('hex');
	}
	return my;
}());

module.exports = crypto;