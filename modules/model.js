  /**********************************/
 /**********  DATA MODELS  *********/
/**********************************/

var DEVICE = function () {
	var my = this;
	my.registration_id = null;
	my.application = null;
	my.platform = null;
        my.user_id = null;
        my.socket = null;

};

var CLIENT = function () {
	var my = this;
	my.socket = null;
	my.service_id = null;
	my.phone = null;
        //my.device = new DEVICE();
};

CLIENT.prototype.DEVICE = {};

// Not on use
var DRIVER = function () {
	var my = {};
	my.socket_id = '';
	my.id = 0;
	my.name = '';
	my.email = '';
	my.phone = 0;
	my.plates = '';
	my.available = 1;
	my.active = 1;
	return my;
};

var VEHICLE = function () {
	var my = {};
	my.socket_id = '';
	my.plates = '';
	my.keylock = '';
	my.model = '';
	my.company_id = 0;
	my.active = 1;
        my.position = null;
	return my;
};

var COMPANY = function () {
	var my = {};
	my.id = 0;
	my.name = '';
	my.phone = '';
	return my;
};

var SERVICE = function (phone) {
	var my = this;
	my.id = null;
	my.vehicle_plates = '';
	my.vehicle_position = '';
	my.client_phone = '';
	my.client_address = '';
	my.client_position = '';
	my.request_hour = new Date().toString();
	my.response_hour = '';
	my.serve_hour = '';
	my.cancel_hour = '';
	my.update_hour = ''; // it's used as partial query text
	my.service_interval = '';
	my.service_type_id = null;
	my.status = 0; //requested
	my.driver_comment = '';	
	my.company_id = null; // company id when request comes from company 
        my.id = my.buildId(phone);
};

SERVICE.prototype.buildId = function(phone) {
                var my = this;
		var now = new Date();
		return "" +
		now.getFullYear() +
		(now.getMonth()+1) +
		now.getDate() +
		now.getHours() +
		now.getMinutes() +
		now.getSeconds() + 
		"_"+phone || my.client_phone;
};

module.exports = { 
	'CLIENT' : CLIENT,
	'DRIVER' : DRIVER,
	'VEHICLE' : VEHICLE,
	'COMPANY' : COMPANY,
	'SERVICE' : SERVICE,
	'DEVICE' : DEVICE
	 }