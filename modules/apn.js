  /***************************************/
 /********** APN SERVER MODULE **********/
/***************************************/
DEBUG="apn";
var MyApn = (function(){
    
        var apn = require('apn');
        
        function MyApn(options) {
            this.apnsConnection = new apn.Connection(options);
            this.registrationIds = [];
            this.apnsConnection.on('error',log('error'));
            this.apnsConnection.on('transmitted', log('transmitted'));
            this.apnsConnection.on('timeout', log('timeout'));
            this.apnsConnection.on('connected', log('connected'));
            this.apnsConnection.on('disconnected', log('disconnected'));
            this.apnsConnection.on('socketError', log('socketError'));
            this.apnsConnection.on('transmissionError', log('transmissionError'));
            this.apnsConnection.on('cacheTooSmall', log('cacheTooSmall')); 
            console.log("Creando un nuevo servidor APN");            
            
        }
        

	
	MyApn.prototype.register = function(registration_id) {
		this.registrationIds.push(registration_id);
	};
	
	MyApn.prototype.unregister = function(registration_id) {
		delete this.registrationIds[registration_id];
	};
	
	MyApn.prototype.send = function(title, text, devices) {
	
		var note = new apn.Notification();
		note.badge = 1;
		note.sound = 'sound/job-done.mp3'; //path to your sound
		note.contentAvailable = 1;

		// You could specify this way
		//note.alert = "Jennifer L commented on your photo:\n Congratulations!! \u270C\u2764\u263A ";

		// Or this way below to set a certain phrase on the button if the user has alert set for the notifications on the app and not just banner
		// and a custom image to show upon launch from the notification.
		note.alert = { "body" : text, "action-loc-key" : "Show Me!" , "launch-image" : "mysplash.png"};

		/* payload property is custom internal use data - use for alert title in my sample app when in the foreground 
		Providers can specify custom payload values outside the Apple-reserved aps namespace. Custom values 
		must use the JSON structured and primitive types: dictionary (object), array, string, number, and Boolean. 
		You should not include customer information (or any sensitive data) as custom payload data. Instead, use it 
		for such purposes as setting context (for the user interface) or internal metrics. For example, a custom payload 
		value might be a conversation identifier for use by an instant-message client app or a timestamp identifying 
		when the provider sent the notification. */

		//note.payload = {'messageFrom': 'Push Notification Sample App'}; // additional payload
		//note.payload = {'rid': roomId}; 
		//note.expiry = Math.floor(Date.now() / 1000) + 3600; 
		console.log("Note " + JSON.stringify(note));
		this.apnsConnection.pushNotification(note, devices);
	};
	
	function log(type) {
		return function() {
                    console.log("Mensaje APN: "+type);
		};
	}


        
	return MyApn;
}());

module.exports = function(options){ 
    return new MyApn(options);
};